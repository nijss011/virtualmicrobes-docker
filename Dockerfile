# Base Image
FROM ubuntu:16.04

# Metadata
LABEL base.image="virtualmicrobes:latest"
LABEL version="1"
LABEL software="VirtualMicrobes"
LABEL software.version="0.0.0"
LABEL description="VirtualMicrobes virtualenv running in Ubuntu 16.04"
LABEL website="https://www.virtualmicrobes.com/virtual-microbes/"
LABEL documentation="https://virtualmicrobes.readthedocs.io/en/latest/"
LABEL license="MIT"
LABEL tags="virtualmicrobes"

ENV DEBIAN_FRONTEND=noninteractive LANG=en_US.UTF-8 LC_ALL=C.UTF-8 LANGUAGE=en_US.UTF-8

RUN apt-get update && apt-get install -y wget curl build-essential git python2.7 libgsl-dev python-dev graphviz
RUN	curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py && python get-pip.py

# Install ETE3 and dependencies
RUN apt install -y python-qt4 python-lxml python-six && \
    pip install numpy ete3==3.0.0b35

# Install VirtualMicrobes
RUN git clone -b develop https://thocu@bitbucket.org/thocu/virtual-microbes.git && \
	pip install -e virtual-microbes