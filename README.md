**Docker container for VirtualMicrobes**

https://www.virtualmicrobes.com/virtual-microbes/

"Virtual Microbes is a computer simulation model to study the eco-evolutionary dynamics of microbes (Thomas Cuypers, 2018). Virtual Microbes is unsupervised, meaning that it targets to combine relevant biological structures (genes, genomes, metabolism, mutations, ecology, etc.) without a preconceived notion of “fitness”, which is instead an emergent phenomenon. By not explicitly defining what the model should do, it allows for a serendipitous approach to study microbial evolution"

**VirtualMicrobes sourcecode:**

https://bitbucket.org/thocu/virtual-microbes/src/develop/ 

**VirtualMicrobes manual:**

https://virtualmicrobes.readthedocs.io/en/latest/